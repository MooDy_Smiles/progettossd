﻿const clienteHOCachePrefix = "clienteHO_";
const serieClientiCacheKey = "serieClienti"; 

function init() {

    // All'avvio dell'applicazione, si esegue il controllo sullo stato della connessione
    checkConnectionOnInit();

    // Recupero degli id dei clienti di cui abbiamo le serie storiche
    popolaClientiStoricizzati();

    // Popolazione della select dei vari tipi di layer da utilizzare
    const sel = document.getElementById("layerTypeSel");
    for (key in LAYER_TYPE) {
        const opt = document.createElement("option");
        opt.text = LAYER_TYPE[key];
        sel.add(opt);
    }
};

function popolaClientiStoricizzati() {

    // Display del loader durante il caricamento
    displayClientiLoader();

    const connectionAvaible = checkStatusConnection();

    // Controllo se sono offline, se lo sono 
    // allora uso il valore cachato nel sessionStorage
    if (!connectionAvaible) {

        // Se la serie che devo recuperare è nel sessionStorage 
        // allora popolo la select, altrimenti non faccio nulla
        if (!sessionStorage.getItem(serieClientiCacheKey)) {
            alert("Servizio al momento non disponibile, torna online per recuperare la lista di clienti");
            hideClientiLoader();
            return;
        } else {
            console.log("Connessione assente ma serie clienti recuperato dalla cache, procedo con la popolazione della select");

            const elencoClienti = JSON.parse(sessionStorage.getItem(serieClientiCacheKey));

            const sel = document.getElementById("idCliente");

            for (key in elencoClienti) {
                const opt = document.createElement("option");
                opt.text = elencoClienti[key];
                sel.add(opt);
            }

            hideClientiLoader();
        }
    } else {

        // Procedo a recuperare la lista dei clienti con uno storico ordini
        const url = "api/clienti/ordini-storicizzati";

        $.ajax({
            url: url,
            type: "GET",
            contentType: "application/json",
            success: function (result) {

                // Cacho sempre i valori, non si sa mai se questi
                // vengano cambiati durante l'esecuzione
                sessionStorage.setItem(serieClientiCacheKey, result);

                const elencoClienti = JSON.parse(result);
                const sel = document.getElementById("idCliente");

                for (key in elencoClienti) {
                    const opt = document.createElement("option");
                    opt.text = elencoClienti[key];
                    sel.add(opt);
                }

                hideClientiLoader();
            },
            error: function (xhr, status, p3, p4) {
                let err = "Error " + " " + status + " " + p3;
                if (xhr.responseText && xhr.responseText[0] == "{")
                    err = JSON.parse(xhr.responseText).message;
                alert(err);

                hideClientiLoader();
            }
        });
    }
}

// Richiesta del calcolo della previsione
function calcolaPrevisione() {
    // Disabilitiamo il bottone per evitare "chiamate" multiple
    const computeBtn = document.getElementById("computeBtn");
    computeBtn.disabled = true;

    // Puliamo il grafico precedente
    cleanChart();

    // Recuperiamo l'id del cliente selezionato
    const idCliente = document.getElementById("idCliente").value;
    const idClienteNum = Number.parseInt(idCliente);

    // Il numero di mesi di cui si vuole ottenere la previsione
    const numPrev = document.getElementById("numPrev").value;
    const numPrevNum = Number.parseInt(numPrev);

    // E il tipo di layer da utilizzare
    const layerType = document.getElementById("layerTypeSel").value;

    // Controllo se idCliente è un numero
    if (Number.isNaN(idClienteNum)) {
        alert("Identificativo Cliente inserito non valido");
        console.log("Identificativo Cliente inserito non valido");
        computeBtn.disabled = false;
        return;
    }

    // Controllo se il numero di previsioni richiesto è un numero
    if (Number.isNaN(numPrevNum) || numPrevNum < 1 || numPrevNum > 5) {
        alert("Numero di previsioni inserito non valido, inserisci un numero tra 1 e 5");
        console.log("Numero di previsioni inserito non valido, inserisci un numero tra 1 e 5");
        computeBtn.disabled = false;
        return;
    }

    // Creo la chiave univoca per la cache dello storico degli ordini
    // per un dato cliente di id idClienteNum
    const clienteCacheKey = clienteHOCachePrefix + idClienteNum;

    displayDataLoader();

    const connectionAvailable = checkStatusConnection();

    // Controllo se sono offline, se lo sono 
    // allora uso il valore cachato nel sessionStorage
    if (!connectionAvailable) {

        // Se lo storico che devo recuperare è nel sessionStorage 
        // allora procedo con la computazione, altrimenti non faccio nulla
        if (!sessionStorage.getItem(clienteCacheKey)) {
            alert("Servizio al momento non disponibile, torna online per effettuare la previsione");
            computeBtn.disabled = false;
            hideDataLoader();
            return;
        } else {
            console.log("Connessione assente ma storico recuperato dalla cache, procedo col calcolo della previsione");
            const data = JSON.parse(sessionStorage.getItem(clienteCacheKey));
            // Eseguiamo la computazione della previsione
            setTimeout(
                function () { doComputation(data, numPrevNum, layerType); },
                500);
        }
    } else {

        // Procedo a recuperare lo storico per il cliente idCliente
        const url = "api/clienti/" + idClienteNum + "/histordini";

        $.ajax({
            url: url,
            type: "GET",
            contentType: "application/json",
            success: function (result) {
                const hOData = JSON.parse(result);

                // Cacho sempre i valori, non si sa mai se questi
                // vengano cambiati durante l'esecuzione
                sessionStorage.setItem(clienteCacheKey, JSON.stringify(hOData));
               // Eseguiamo la computazione della previsione
                doComputation(hOData, numPrevNum, layerType);
            },
            error: function (xhr, status, p3, p4) {
                let err = "Error " + " " + status + " " + p3;
                if (xhr.responseText && xhr.responseText[0] == "{")
                    err = JSON.parse(xhr.responseText).message;
                alert(err);
                computeBtn.disabled = false;
                hideDataLoader();
            }
        });
    }    
}

// Richiamiamo la funzione per la computazione del file prediction.js
function doComputation(data, numPrevNum, layerType) {
    computePrevision(data, numPrevNum, layerType)
        .then(function () {
            console.log("Terminato con successo");
            computeBtn.disabled = false;
            hideDataLoader();
        })
        .catch(function (err) {
            console.log("Errore: " + err);
            computeBtn.disabled = false;
            hideDataLoader();
            alert("Previsione fallita " + err);
        });
}

// Funzioni per visualizzazione loader clienti
function displayClientiLoader() {
    const clientiWrapper = document.getElementById("idClienteWrapper");
    const clientiLoader = document.getElementById("idClienteLoader");

    clientiWrapper.classList.add("d-none");
    clientiLoader.classList.remove("d-none");
}

function hideClientiLoader() {
    const clientiWrapper = document.getElementById("idClienteWrapper");
    const clientiLoader = document.getElementById("idClienteLoader");

    clientiWrapper.classList.remove("d-none");
    clientiLoader.classList.add("d-none");
}

// Funzioni per visualizzazione loader previsione
function displayDataLoader() {
    const dataLoader = document.getElementById("dataLoader");

    dataLoader.classList.remove("d-none");
}

function hideDataLoader() {
    const dataLoader = document.getElementById("dataLoader");

    dataLoader.classList.add("d-none");
}

// Funzione di display dell'errore per connessione assente
function displayConnectionError() {
    const navbar = document.querySelector('#navbar');

    if (navbar) {
        navbar.classList.remove('d-none');
    }    
}

// Funzione per nascondere l'errore di connessione assente
function hideConnectionError() {
    const navbar = document.querySelector('#navbar');

    if (navbar) {
        navbar.classList.add('d-none');
    }
}

// Carico il service worker
if (navigator.serviceWorker && navigator.serviceWorker.controller) {
    console.log('Active service worker found, no need to register');
} else {
    window.addEventListener('load', function () {
        navigator.serviceWorker.register('./serviceWorkers.js', { scope: './' }).then(function (registration) {
            // Registration was successful
            console.log('ServiceWorker registration successful with scope: ', registration.scope);
        }, function (err) {
            // registration failed :(
            console.log('ServiceWorker registration failed: ', err);
        });
    });
}

function checkConnectionOnInit() {
    // Quando si carica la prima volta, se è offline devo mostrare il messaggio di errore
    const connectionAvaible = checkStatusConnection();

    if (connectionAvaible) {
        console.log("Sei online");

        hideConnectionError();
    } else {
        console.log("Sei offline");

        displayConnectionError();
    }
}

function checkStatusConnection() {
    if (navigator.onLine) {
        return true;
    }

    return false;
}

// Mi metto in listen su quando sono online/offline
window.addEventListener('online', function (e) {
    console.log("Sei online");

    hideConnectionError();

}, false);

window.addEventListener('offline', function (e) {
    console.log("Sei offline");

    displayConnectionError();

}, false);