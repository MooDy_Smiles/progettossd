﻿google.charts.load('current', { packages: ['corechart', 'line'] });
//google.charts.setOnLoadCallback(drawLineColors);

var data;
var chart;
const options = {
    legend: {
        position: 'bottom'
    },
    height: 350
};

function initGrafico(elencoStorico, elencoMesi) {
    data = new google.visualization.DataTable();
    data.addColumn('number', 'Mese');
    data.addColumn('number', 'Storico');
    data.addColumn('number', 'Previsione');

    const elencoPunti = [];
    for (var i = 0; i < elencoMesi.length; i++) {
        elencoPunti.push([elencoMesi[i], elencoStorico[i], null]);
    }
    data.addRows(elencoPunti);
    // l'opzione legend bottom non è ancora supportata dai Material Charts
    //chart = new google.charts.Line(document.getElementById('histOPlot'));
    //chart.draw(data, google.charts.Line.convertOptions(options));
    chart = new google.visualization.LineChart(document.getElementById('histOPlot'));
    chart.draw(data, options);
}

function addPrevToGrafico(prev, mese) {
    data.addRows([[mese, null, prev]]);
    //chart.draw(data, google.charts.Line.convertOptions(options));
    chart.draw(data, options);
}

function cleanChart() {
    chart = null;
    data = null;
    $('#histOPlot').empty();
}

$(window).resize(function () {
    if(chart != null) {
        chart.draw(data, options);
    }
});