﻿
const LAYER_TYPE = {
    GRU: 'GRU',
    SIMPLE_RNN: 'SIMPLE_RNN',
    LSTM: 'LSTM'
};

async function computePrevision(histOrdini, numPrev, layerType) {
    console.log('computePrevision inizio!');

    var arrayOrdiniNorm     = [];
    var arrayMeseNorm       = [];
    var arrayOrdini         = [];
    var arrayMese           = [];
    var arrayValidationOrd  = [];
    var arrayValidationPer  = [];
    var maxVal              = null;
    var minVal              = null;
    var maxPeriodo          = null;
    var minPeriodo          = null;
    var sampleNumber        = null;

    if (histOrdini == null || numPrev < 1) {
        throw 'Input non valido';
    }
    
    for (var i = 0; i < histOrdini.length; i++) {
        if (histOrdini[i] != null) {
            if (maxVal == null || histOrdini[i].val > maxVal) {
                maxVal = histOrdini[i].val;
            }
            if (minVal == null || histOrdini[i].val < minVal) {
                minVal = histOrdini[i].val;
            }
            if (maxPeriodo == null || histOrdini[i].periodo > maxPeriodo) {
                maxPeriodo = histOrdini[i].periodo;
            }
            if (minPeriodo == null || histOrdini[i].periodo < minPeriodo) {
                minPeriodo = histOrdini[i].periodo;
            }
        }
    }

    // Prendiamo in considerazione anche i periodi utilizzati per la previsione
    maxPeriodo = maxPeriodo + numPrev;
    
    for (var i = 0, j = 0, k = 0; i < histOrdini.length; i++) {
        if (histOrdini[i] != null) {
            if (i % 10 == 0) {
                arrayValidationOrd[k] = normalize(histOrdini[i].val, maxVal, minVal);
                arrayValidationPer[k] = normalize(histOrdini[i].periodo, maxPeriodo, minPeriodo);
                k += 1;
            } else {
                arrayOrdiniNorm[j]  = normalize(histOrdini[i].val, maxVal, minVal);
                arrayMeseNorm[j]    = normalize(histOrdini[i].periodo, maxPeriodo, minPeriodo);
                j += 1;
            }
            arrayOrdini[j+k] = histOrdini[i].val;
            arrayMese[j+k] = histOrdini[i].periodo;
        }
    }

    // The central unit of data in TensorFlow.js is the tensor: a set of numerical values shaped into an array of one or more dimensions.
    // A Tensor instance has a shape attribute that defines the array shape
    const xs = tf.tensor3d(arrayMeseNorm, [arrayOrdiniNorm.length, 1, 1]);
    const ys = tf.tensor2d(arrayOrdiniNorm, [arrayOrdiniNorm.length, 1]);

    const xsVal = tf.tensor3d(arrayValidationPer, [arrayValidationPer.length, 1, 1]);
    const ysVal = tf.tensor2d(arrayValidationOrd, [arrayValidationOrd.length, 1]);

    // creazione iniziale del grafico con valori appartenti allo storico
    initGrafico(arrayOrdini, arrayMese);

    // Models are one of the primary abstractions used in TensorFlow.js Layers. Models can be trained, evaluated, and used for prediction.
    // Models are a collection of Layers, There are two primary ways of creating models:
    //      Sequential — Easiest, works if the models is a simple stack of each layer's input resting on the top of the previous layer's output.
    //      Model — Offers more control if the layers need to be wired together in graph-like ways — multiple 'towers', layers that skip a layer, etc.
    // The key difference between tf.model() and tf.sequential() is that tf.sequential() is less generic, supporting only a linear stack of layers. tf.model() 
    // is more generic and supports an arbitrary graph(without cycles) of layers.
    const model = tf.sequential();

    // Layers are the primary building block for constructing a Model. Each layer will typically perform some computation to transform its input to its output.
    var firstLayerOption = {
        units: 50,                  // units (number) Positive integer, dimensionality of the output space. 
        inputShape: [1, 1],         // input dimensions
        returnSequences: true       // set returnSequens to true means this layer output is always set into the previus layer
    };
    var firstLayer;
    switch (layerType) {

        // Gated recurrent units (GRUs) are a gating mechanism in recurrent neural networks, introduced in 2014 by Kyunghyun Cho et al.
        // Their performance on polyphonic music modeling and speech signal modeling was found to be similar to that of long short - term memory(LSTM).
        // However, GRUs have been shown to exhibit better performance on smaller datasets.
        case LAYER_TYPE.GRU:
            firstLayer = tf.layers.gru(firstLayerOption);
            break;

        // Long-Short Term Memory layer - Hochreiter 1997.
        // The expression long short-term refers to the fact that LSTM is a model for the short-term memory which can last for a long period of time.
        // An LSTM is well - suited to classify, process and predict time series given time lags of unknown size and duration between important events.
        case LAYER_TYPE.LSTM:
            firstLayer = tf.layers.lstm(firstLayerOption);
            break;

        // Fully-connected RNN where the output is to be fed back to input.
        // Operates on a sequence of inputs. The shape of the input needs to be at least 2-D, with the first dimension being time steps
        case LAYER_TYPE.SIMPLE_RNN:
            firstLayer= tf.layers.simpleRNN(firstLayerOption);
            break;
        default:
            throw 'Layer non valido';
    }
    model.add(firstLayer);

    // Applies dropout to the input.
    // Dropout consists in randomly setting a fraction rate of input units to 0 at each update during training time, which helps prevent overfitting.
    model.add(
        tf.layers.dropout({
            rate: 0.2           // Float between 0 and 1. Fraction of the input units to drop. 
        })
    );

    var middleLayerOption = {
        units: 100,
        returnSequences: false
    };
    var middleLayer;
    switch (layerType) {
        case LAYER_TYPE.GRU:
            middleLayer = tf.layers.gru(middleLayerOption);
            break;
        case LAYER_TYPE.LSTM:
            middleLayer = tf.layers.lstm(middleLayerOption);
            break;
        case LAYER_TYPE.SIMPLE_RNN:
            middleLayer = tf.layers.simpleRNN(middleLayerOption);
            break;
        default:
            throw 'Layer non valido';
    }
    model.add(middleLayer);
    
    model.add(
        tf.layers.dropout({
            rate: 0.2
        })
    );

    // Creates a dense (fully connected) layer.
    // Used to aggregate the data into one single value
    model.add(
        tf.layers.dense({
            units: 1
        })
    );

    // Applies an activation function to an output.
    // This layer applies element-wise activation function. Other layers, notably dense can also apply activation functions.
    // Use this isolated activation function to extract the values before and after the activation.
    model.add(
        tf.layers.activation({
            activation: 'linear'
            // ('elu'|'hardSigmoid'|'linear'|'relu'|'relu6'| 'selu'|'sigmoid'|'softmax'|'softplus'|'softsign'|'tanh'|string)
            // Name of the activation function to use. 
        })
    );

    // Configures and prepares the model for training and evaluation. Compiling outfits the model with an optimizer, loss, and/or metrics. 
    // Calling fit or evaluate on an un - compiled model will throw an error.
    model.compile({
        loss: 'meanSquaredError',           // A loss function, which measures how well a given polynomial fits the data. The lower the loss value, the better the polynomial fits the data.
                                            // If the model has multiple outputs, you can use a different loss on each output by passing a dictionary or an Array of losses
                                            // Computes the mean squared error between two tensors.

        optimizer: 'rmsprop',               // An optimizer, which implements an algorithm for revising our coefficient values based on the output of the loss function.
                                            // The optimizer's goal is to minimize the output value of the loss function.
                                            // Constructs a AdamOptimizer that uses the Adam algorithm.
                                            // learningRate (number) The learning rate to use for the Adam gradient descent algorithm.
                                            // ('sgd'|tf.train.adam(0.001)|...)

        metrics: ['accuracy']               // List of metrics to be evaluated by the model during training and testing.
    });
    

    // Trains the model for a fixed number of epochs (iterations on a dataset).
    const foo = model.fit(xs, ys, {
        batchSize: 34,                      // How many examples the model should "see" before making a parameter update
        epochs: 50,                         // Number of times to iterate over the training data arrays
        validationData: [xsVal, ysVal]      // Data on which to evaluate the loss and any model metrics at the end of each epoch. The model will not be trained on this data.
        /*
        callbacks: {                           List of callbacks to be called during training.
            onBatchEnd: asyn () => {           callbacks (BaseCallback[]|CustomCallbackConfig|CustomCallbackConfig[])
                * post Batch operations *
            }
        }*/
    });
    foo.then(
        function (data) { },
        function (data) {
            throw data;
        }
    );

    const h = await foo;
    console.log('acc: ' + h.history.acc + ', loss: ' + h.history.loss);

    // tensor creation for prediction
    const elencoMesiPrevNorm    = [];
    const elencoMesiPrev        = [];
    for (var i = 1; i <= numPrev; i++) {
        elencoMesiPrevNorm[i - 1]   = normalize(arrayMese[arrayMese.length - 1] + i, maxPeriodo, minPeriodo);
        elencoMesiPrev[i - 1]       = arrayMese[arrayMese.length - 1] + i;
    }

    const prevMesi = tf.tensor3d(elencoMesiPrevNorm, [elencoMesiPrevNorm.length, 1, 1]);

    // Generates output predictions for the input samples.
    const previsione = await model.predict(prevMesi).data();

    // Aggiungiamo l'ultimo valore dello storico in modo da avere una continuità nel grafico
    addPrevToGrafico(arrayOrdini[arrayOrdini.length - 1], arrayMese[arrayMese.length - 1]);

    for (var i = 0; i < elencoMesiPrev.length; i++) {
        // add the preditcion to the chart
        addPrevToGrafico(Math.round(denormalize(previsione[i], maxVal, minVal)), elencoMesiPrev[i]);
    }

    console.log('computePrevision fine');
}

// utility function to manage input normalization

function normalize(num, max, min) {
    return (num - min) * (1 / (max - min));
}

function denormalize(num, max, min) {
    return (num / (1 / (max - min))) + min;
}
