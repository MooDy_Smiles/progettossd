﻿// utilizzo cacheName per cache versioning
var cacheName = 'ssdPruccoliMaestri';

// installazione: metto in cache elementi statici (per funzionamento offline)
self.addEventListener('install', function (e) {
    e.waitUntil(
        caches.open(cacheName).then(function (cache) {
            return cache.addAll([
                '/index.html',
                '/style/main.css',
                '/javascript/main.js',
                '/javascript/prediction.js',
                '/javascript/chart.js',
            ]).then(function () {
                // Forzo il nuovo serviceWorker a diventare il sw attivo
                self.skipWaiting();
            });
        })
    );
});

// quando il browser legge un url
self.addEventListener('fetch', function (event) {
    // ritorno oggetto cachato, oppure eseguo normalmente il fetching
    event.respondWith(
        caches.match(event.request).then(function (response) {
            if (response) {
                // prendo da cache
                return response;
            }
            // fetch normale
            return fetch(event.request);
        })
    );
});