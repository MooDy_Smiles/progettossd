﻿using System.Linq;
using System.Web.Http;
using Newtonsoft.Json;

namespace ProgettoSSDPruccoli.Controllers
{
    public class HistOrdiniController : ApiController
    {
        // GET api/<controller>/<action>
        [HttpGet]
        [ActionName("GetAllHistOrdini")]
        public string GetAllHistOrdini()
        {
            ordini2017Entities context = new ordini2017Entities();

            string res = JsonConvert.SerializeObject(context.histordini);

            return res;
        }

        // GET api/<controller>
        [HttpGet]
        [Route("api/clienti/{idCliente:int}/histordini")]
        public string FindHistOrdiniByIdCliente(int idCliente)
        {
            ordini2017Entities context = new ordini2017Entities();
            
            string res = JsonConvert.SerializeObject(context.histordini.Where(ho => ho.idserie == idCliente));

            return res;
        }

        // GET api/<controller>
        [HttpGet]
        [Route("api/clienti/ordini-storicizzati")]
        public string FindElencoSerieDisponibili()
        {
            ordini2017Entities context = new ordini2017Entities();

            var elencoSerie = context.histordini.Select(m => m.idserie).Distinct().OrderBy(x => x.Value);
           
            return JsonConvert.SerializeObject(elencoSerie);
        }

    }
}