﻿using System.Web.Http;
using Newtonsoft.Json;

namespace ProgettoSSDPruccoli.Controllers
{
    public class CostiController : ApiController
    {
        // GET api/<controller>
        [HttpGet]
        [ActionName("GetAllCosti")]
        public string GetAllCosti()
        {
            ordini2017Entities context = new ordini2017Entities();

            string res = JsonConvert.SerializeObject(context.costi);

            return res;
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}